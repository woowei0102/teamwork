var a = 0;
var time = 60;
var t;
var move;
var speed = 1;
var pause = 0;
var rand = Math.floor(Math.random()*3)+1;
var player1_name;
var player2_name;
var player3_name;
var select_ghost = 0;
var select_ghost1 = 0;
var select_ghost2 = 0;
var select_ghost3 = 0;
var p1 = document.getElementById("move_player1");
var p2 = document.getElementById("move_player2");
var p3 = document.getElementById("move_player3");
var key_pressed=[];
var key;
var player1_safe = 0;
var player2_safe = 0;
var player3_safe = 0;
var player1_count = 0;
var player2_count = 0;
var player3_count = 0;
var x_p1, y_p1;
var x_p2, y_p2;
var x_p3, y_p3;
var coordinate;

function playername() {
	const name = document.getElementById("name_text").value;
	if (a == 0) {
		document.getElementById("player1").innerHTML = name;
		player1_name = name;
		document.getElementById("title_name").innerHTML =
			" Enter Player2's name";
		document.getElementById("img_player1").innerHTML = name;
		document.getElementById("name_text").value = "player2";
	} else if (a == 1) {
		document.getElementById("player2").innerHTML = name;
		player2_name = name;
		document.getElementById("title_name").innerHTML =
            " Enter Player3's name";
		document.getElementById("img_player2").innerHTML = name;
		document.getElementById("name_text").value = "player3";
	} else {
		document.getElementById("player3").innerHTML = name;
		player3_name = name;
        document.getElementById("Allname").style =
			"visibility:hidden";
		document.getElementById("img_player3").innerHTML = name;
    }
    a++;
}

function m1() {
	pause = 0;
	document.getElementById("ghost_png").style = 'visibility:hidden';
	if((rand==1)&&(select_ghost==0)){
		document.getElementById("player1_png").src='ghost.png';
		select_ghost=1;
		select_ghost1=1;
	}
	else if((rand==2)&&(select_ghost==0)){
		document.getElementById("player2_png").src='ghost.png';
		select_ghost=1;
		select_ghost2=1;
	}
	else if((rand==3)&&(select_ghost==0)){
		document.getElementById("player3_png").src='ghost.png';
		select_ghost=1;
		select_ghost3=1;
	}	
		
	
	if(time<=0){
		time=-1;
		clearTimeout(t);
		document.getElementById("time").innerHTML = 0;
		document.getElementById("settlement_time").innerHTML = 0;
		if(select_ghost1){
			if(player2_count < player3_count){
				document.getElementById("Winner").innerHTML = player2_name;
				document.getElementById("Loser").innerHTML = player1_name;	
			}
			else if(player2_count > player3_count){
				document.getElementById("Winner").innerHTML = player3_name;
				document.getElementById("Loser").innerHTML = player1_name;	
			}
			else{
				document.getElementById("Winner").innerHTML = player1_name + ' ' + player3_name;
				document.getElementById("Loser").innerHTML = player1_name;				
			}
		}
		else if(select_ghost2){
			if(player1_count < player3_count){
				document.getElementById("Winner").innerHTML = player1_name;
				document.getElementById("Loser").innerHTML = player2_name;	
			}
			else if(player1_count > player3_count){
				document.getElementById("Winner").innerHTML = player3_name;
				document.getElementById("Loser").innerHTML = player2_name;	
			}
			else{
				document.getElementById("Winner").innerHTML = player1_name + ' ' + player3_name;
				document.getElementById("Loser").innerHTML = player2_name;				
			}
		}
		else if(select_ghost3){
			if(player1_count < player2_count){
				document.getElementById("Winner").innerHTML = player1_name;
				document.getElementById("Loser").innerHTML = player3_name;	
			}
			else if(player1_count > player2_count){
				document.getElementById("Winner").innerHTML = player2_name;
				document.getElementById("Loser").innerHTML = player3_name;	
			}
			else{
				document.getElementById("Winner").innerHTML = player1_name + ' ' + player2_name;
				document.getElementById("Loser").innerHTML = player3_name;				
			}
		}
		document.getElementById("settlement").style = 'visibility:visible';
		document.getElementById("bg").style ='background-color:red'; 
	}
	else{
		document.getElementById("time").innerHTML = time;
		
		t = setTimeout(function () {
			m1();
		}, 1000);
		
		document.addEventListener("keydown",function(e){
			key_pressed[e.keyCode] = true;
		});
		document.addEventListener("keyup",function(e){
			key_pressed[e.keyCode] = false;
		});
		
		move = setInterval(function () {
			if(time==-1){
				clearInterval(move);
			}
			else if(pause == 0){
				for(key in key_pressed){
					if(key_pressed[key]){
						
						// 取得座標
						[x_p1, y_p1] = getCoordinate(p1);
						[x_p2, y_p2] = getCoordinate(p2);
						[x_p3, y_p3] = getCoordinate(p3);

						if (key == 87) { // W上 
							
							// 如果 Player 1 是鬼，限制它向上走
							if(select_ghost1) {

								// 限制向上走進 「 SAFE 1 」
								if( -2 <= x_p1 && x_p1 <= 158) {
									if(y_p1 <= 110) {
										break;
									}
								}

								// 限制向上走進 「 SAFE 2 」
								if(700 <= x_p1 && x_p1 <=902) {
									if(y_p1 <= 110) {
										break;
									}
								}

							}

							if(p1.offsetTop>=-35){
								p1.style.top=parseInt(p1.style.top)-speed +'px';
							}	
						}

						if (key == 83) { // S下

							if(select_ghost1) {
								
								// 限制向下走進 「 SAFE 3 」
								if( -2 <= x_p1 && x_p1 <= 158) {
									if( 500 <= y_p1) {
										break;
									}
								}

								// 限制向下走進 「 SAFE 4 」
								if(700 <= x_p1 && x_p1 <=902) {
									if( 500 <= y_p1) {
										break;
									}
								}
							}

							if(p1.offsetTop<=650){
								p1.style.top=parseInt(p1.style.top)+speed +'px';
							}
						}

						if (key == 65) { // A左

							// 如果 Player 1 是鬼，限制它向「 左 」
							if(select_ghost1) {

								// 限制向「 左 」進入「 SAFE 1 」
								if( -36 <= y_p1 && y_p1 <= 100) {
									if(x_p1 <= 190) {
										break;
									}
								}

								// 限制向「 左 」進入「 SAFE 4 」
								if( 504 <= y_p1 && y_p1 <= 652) {
									if(x_p1 <= 190) {
										break;
									}
								}

							}

							if(p1.offsetLeft>=0){
								p1.style.left=parseInt(p1.style.left)-speed +'px';	
							}
						}
						if (key == 68) { // D右

							// 如果 Player 1 是鬼，限制它向「 右 」
							if(select_ghost1){
								
								// 限制向右進入 「 SAFE 2」
								if( -36 <= y_p1 && y_p1 <= 100){
									if(678 <= x_p1) {
										break;
									}
								}
								
								// 限制向右進入 「 SAFE 4 」
								if( 504 <= y_p1 && y_p1 <= 652){
									if(678 <= x_p1) {
										break;
									}
								}

							}

							if(p1.offsetLeft<=900){
								p1.style.left=parseInt(p1.style.left)+speed +'px';
							}
						}
						
						if (key == 73) { // I上 
							
							// 如果 Player 2 是鬼，限制它向「 上 」
							if(select_ghost2) {

								// 限制向上走進 「 SAFE 1 」
								if( -2 <= x_p2 && x_p2 <= 158) {
									if(y_p2 <= 110) {
										break;
									}
								}

								// 限制向上走進 「 SAFE 2 」
								if(700 <= x_p2 && x_p2 <=902) {
									if(y_p2 <= 110) {
										break;
									}
								}

							}

							if(p2.offsetTop>=-35){
								p2.style.top=parseInt(p2.style.top)-speed +'px';
							}
						}
						if (key == 75) { // K下

							// 如果 Player 2 是鬼，限制它向「 下 」
							if(select_ghost2) {
								
								// 限制向下走進 「 SAFE 3 」
								if( -2 <= x_p2 && x_p2 <= 158) {
									if( 500 <= y_p2) {
										break;
									}
								}

								// 限制向下走進 「 SAFE 4 」
								if(700 <= x_p2 && x_p2 <=902) {
									if( 500 <= y_p2) {
										break;
									}
								}
							}

							if(p2.offsetTop<=650){
								p2.style.top=parseInt(p2.style.top)+speed +'px';
							}
						}
						if (key == 74) { // J左

							// 如果 Player 2 是鬼，限制它向「 左 」
							if(select_ghost2) {

								// 限制向「 左 」進入「 SAFE 1 」
								if( -36 <= y_p2 && y_p2 <= 100) {
									if(x_p2 <= 190) {
										break;
									}
								}

								// 限制向「 左 」進入「 SAFE 4 」
								if( 504 <= y_p2 && y_p2 <= 652) {
									if(x_p2 <= 190) {
										break;
									}
								}

							}

							if(p2.offsetLeft>=0){
								p2.style.left=parseInt(p2.style.left)-speed+'px';
							}
						}
						if (key == 76) { // L右

							// 如果 Player 2 是鬼，限制它向「 右 」
							if(select_ghost2){
								
								// 限制向右進入 「 SAFE 2」
								if( -36 <= y_p2 && y_p2 <= 100){
									if(678 <= x_p2) {
										break;
									}
								}
								
								// 限制向右進入 「 SAFE 4 」 3?
								if( 504 <= y_p2 && y_p2 <= 652){
									if(678 <= x_p2) {
										break;
									}
								}

							}
							
							if(p2.offsetLeft<=900){
								p2.style.left=parseInt(p2.style.left)+speed+'px';
							}
						}
						
						if (key == 38) { // 方向鍵上 

							// 如果 Player 3 是鬼，限制它向「 上 」
							if(select_ghost3) {

								// 限制向上走進 「 SAFE 1 」
								if( -2 <= x_p3 && x_p3 <= 158) {
									if(y_p3 <= 110) {
										break;
									}
								}

								// 限制向上走進 「 SAFE 2 」
								if(700 <= x_p3 && x_p3 <=902) {
									if(y_p3 <= 110) {
										break;
									}
								}

							}

							if(p3.offsetTop>=-35){
								p3.style.top=parseInt(p3.style.top)-speed +'px';
							}
						}
						if (key == 40) { // 方向鍵下

							// 如果 Player 3 是鬼，限制它向「 下 」
							if(select_ghost3) {
								
								// 限制向下走進 「 SAFE 3 」
								if( -2 <= x_p3 && x_p3 <= 158) {
									if( 500 <= y_p3) {
										break;
									}
								}

								// 限制向下走進 「 SAFE 4 」
								if(700 <= x_p3 && x_p3 <=902) {
									if( 500 <= y_p3) {
										break;
									}
								}
							}

							if(p3.offsetTop<=650){
								p3.style.top=parseInt(p3.style.top)+speed +'px';
							}
						}
						if (key == 37) { // 方向鍵左

							// 如果 Player 3 是鬼，限制它向「 左 」
							if(select_ghost3) {

								// 限制向「 左 」進入「 SAFE 1 」
								if( -36 <= y_p3 && y_p3 <= 100) {
									if(x_p3 <= 190) {
										break;
									}
								}

								// 限制向「 左 」進入「 SAFE 4 」
								if( 504 <= y_p3 && y_p3 <= 652) {
									if(x_p3 <= 190) {
										break;
									}
								}

							}

							if(p3.offsetLeft>=0){
								p3.style.left=parseInt(p3.style.left)-speed+'px';
							}
						}
						if (key == 39) { // 方向鍵右

							// 如果 Player 3 是鬼，限制它向「 右 」
							if(select_ghost3){
								
								// 限制向右進入 「 SAFE 2」
								if( -36 <= y_p3 && y_p3 <= 100){
									if(678 <= x_p3) {
										break;
									}
								}
								
								// 限制向右進入 「 SAFE 4 」
								if( 504 <= y_p3 && y_p3 <= 652){
									if(678 <= x_p3) {
										break;
									}
								}

							}

							if(p3.offsetLeft <=900){
								p3.style.left=parseInt(p3.style.left)+speed+'px';
							}
						}
						
						if(select_ghost1==1){
							if(
							(((p1.offsetTop<=p2.offsetTop)&&((p1.offsetTop+80)>=p2.offsetTop))
								&&((p1.offsetLeft<=p2.offsetLeft)&&((p1.offsetLeft+60)>=p2.offsetLeft)))
							||(((p1.offsetTop<=p2.offsetTop)&&((p1.offsetTop+80)>=p2.offsetTop))
								&&((p1.offsetLeft<=(p2.offsetLeft+60))&&((p1.offsetLeft+60)>=(p2.offsetLeft+60))))
							||(((p1.offsetTop<=(p2.offsetTop+80))&&((p1.offsetTop+80)>=(p2.offsetTop+80)))
								&&((p1.offsetLeft<=(p2.offsetLeft))&&((p1.offsetLeft+60)>=(p2.offsetLeft))))
							||(((p1.offsetTop<=(p2.offsetTop+80))&&((p1.offsetTop+80)>=(p2.offsetTop+80)))
								&&((p1.offsetLeft<=(p2.offsetLeft+60))&&((p1.offsetLeft+60)>=(p2.offsetLeft+60))))
							)
							{
								clearTimeout(t);
								clearInterval(move);
								pause = 1;
								document.getElementById("bg").style ='background-color:red';
								document.getElementById("settlement").style = 'visibility:visible';
								document.getElementById("settlement_time").innerHTML = time+1;
								document.getElementById("Winner").innerHTML = player1_name;
								document.getElementById("Loser").innerHTML= player2_name;
								
							}
							else if(
							(((p1.offsetTop<=p3.offsetTop)&&((p1.offsetTop+80)>=p3.offsetTop))
								&&((p1.offsetLeft<=p3.offsetLeft)&&((p1.offsetLeft+60)>=p3.offsetLeft)))
							||(((p1.offsetTop<=p3.offsetTop)&&((p1.offsetTop+80)>=p3.offsetTop))
								&&((p1.offsetLeft<=(p3.offsetLeft+60))&&((p1.offsetLeft+60)>=(p3.offsetLeft+60))))
							||(((p1.offsetTop<=(p3.offsetTop+80))&&((p1.offsetTop+80)>=(p3.offsetTop+80)))
								&&((p1.offsetLeft<=(p3.offsetLeft))&&((p1.offsetLeft+60)>=(p3.offsetLeft))))
							||(((p1.offsetTop<=(p3.offsetTop+80))&&((p1.offsetTop+80)>=(p3.offsetTop+80)))
								&&((p1.offsetLeft<=(p3.offsetLeft+60))&&((p1.offsetLeft+60)>=(p3.offsetLeft+60))))
							)
							{
								clearTimeout(t);
								clearInterval(move);
								pause = 1;
								document.getElementById("bg").style ='background-color:red';
								document.getElementById("settlement").style = 'visibility:visible';
								document.getElementById("settlement_time").innerHTML = time+1;
								document.getElementById("Winner").innerHTML = player1_name;
								document.getElementById("Loser").innerHTML = player3_name;	
							}
						}
						else if(select_ghost2==1){
							if(
							(((p2.offsetTop<=p1.offsetTop)&&((p2.offsetTop+80)>=p1.offsetTop))
								&&((p2.offsetLeft<=p1.offsetLeft)&&((p2.offsetLeft+60)>=p1.offsetLeft)))
							||(((p2.offsetTop<=p1.offsetTop)&&((p2.offsetTop+80)>=p1.offsetTop))
								&&((p2.offsetLeft<=(p1.offsetLeft+60))&&((p2.offsetLeft+60)>=(p1.offsetLeft+60))))
							||(((p2.offsetTop<=(p1.offsetTop+80))&&((p2.offsetTop+80)>=(p1.offsetTop+80)))
								&&((p2.offsetLeft<=(p1.offsetLeft))&&((p2.offsetLeft+60)>=(p1.offsetLeft))))
							||(((p2.offsetTop<=(p1.offsetTop+80))&&((p2.offsetTop+80)>=(p1.offsetTop+80)))
								&&((p2.offsetLeft<=(p1.offsetLeft+60))&&((p2.offsetLeft+60)>=(p1.offsetLeft+60))))
							)
							{
								clearTimeout(t);
								clearInterval(move);
								pause = 1;
								document.getElementById("bg").style ='background-color:red';
								document.getElementById("settlement").style = 'visibility:visible';
								document.getElementById("settlement_time").innerHTML = time+1;
								document.getElementById("Winner").innerHTML = player2_name;
								document.getElementById("Loser").innerHTML= player1_name;
								
							}
							else if(
							(((p2.offsetTop<=p3.offsetTop)&&((p2.offsetTop+80)>=p3.offsetTop))
								&&((p2.offsetLeft<=p3.offsetLeft)&&((p2.offsetLeft+60)>=p3.offsetLeft)))
							||(((p2.offsetTop<=p3.offsetTop)&&((p2.offsetTop+80)>=p3.offsetTop))
								&&((p2.offsetLeft<=(p3.offsetLeft+60))&&((p2.offsetLeft+60)>=(p3.offsetLeft+60))))
							||(((p2.offsetTop<=(p3.offsetTop+80))&&((p2.offsetTop+80)>=(p3.offsetTop+80)))
								&&((p2.offsetLeft<=(p3.offsetLeft))&&((p2.offsetLeft+60)>=(p3.offsetLeft))))
							||(((p2.offsetTop<=(p3.offsetTop+80))&&((p2.offsetTop+80)>=(p3.offsetTop+80)))
								&&((p2.offsetLeft<=(p3.offsetLeft+60))&&((p2.offsetLeft+60)>=(p3.offsetLeft+60))))
							)
							{
								clearTimeout(t);
								clearInterval(move);
								pause = 1;
								document.getElementById("bg").style ='background-color:red';
								document.getElementById("settlement").style = 'visibility:visible';
								document.getElementById("settlement_time").innerHTML = time+1;
								document.getElementById("Winner").innerHTML = player2_name;
								document.getElementById("Loser").innerHTML = player3_name;	
							}
						}
						else if(select_ghost3==1){
							if(
							(((p3.offsetTop<=p1.offsetTop)&&((p3.offsetTop+80)>=p1.offsetTop))
								&&((p3.offsetLeft<=p1.offsetLeft)&&((p3.offsetLeft+60)>=p1.offsetLeft)))
							||(((p3.offsetTop<=p1.offsetTop)&&((p3.offsetTop+80)>=p1.offsetTop))
								&&((p3.offsetLeft<=(p1.offsetLeft+60))&&((p3.offsetLeft+60)>=(p1.offsetLeft+60))))
							||(((p3.offsetTop<=(p1.offsetTop+80))&&((p3.offsetTop+80)>=(p1.offsetTop+80)))
								&&((p3.offsetLeft<=(p1.offsetLeft))&&((p3.offsetLeft+60)>=(p1.offsetLeft))))
							||(((p3.offsetTop<=(p1.offsetTop+80))&&((p3.offsetTop+80)>=(p1.offsetTop+80)))
								&&((p3.offsetLeft<=(p1.offsetLeft+60))&&((p3.offsetLeft+60)>=(p1.offsetLeft+60))))
							)
							{
								clearTimeout(t);
								clearInterval(move);
								pause = 1;
								document.getElementById("bg").style ='background-color:red';
								document.getElementById("settlement").style = 'visibility:visible';
								document.getElementById("settlement_time").innerHTML = time+1;
								document.getElementById("Winner").innerHTML = player3_name;
								document.getElementById("Loser").innerHTML= player1_name;
								
							}
							else if(
							(((p3.offsetTop<=p2.offsetTop)&&((p3.offsetTop+80)>=p2.offsetTop))
								&&((p3.offsetLeft<=p2.offsetLeft)&&((p3.offsetLeft+60)>=p2.offsetLeft)))
							||(((p3.offsetTop<=p2.offsetTop)&&((p3.offsetTop+80)>=p2.offsetTop))
								&&((p3.offsetLeft<=(p2.offsetLeft+60))&&((p3.offsetLeft+60)>=(p2.offsetLeft+60))))
							||(((p3.offsetTop<=(p2.offsetTop+80))&&((p3.offsetTop+80)>=(p2.offsetTop+80)))
								&&((p3.offsetLeft<=(p2.offsetLeft))&&((p3.offsetLeft+60)>=(p2.offsetLeft))))
							||(((p3.offsetTop<=(p2.offsetTop+80))&&((p3.offsetTop+80)>=(p2.offsetTop+80)))
								&&((p3.offsetLeft<=(p2.offsetLeft+60))&&((p3.offsetLeft+60)>=(p2.offsetLeft+60))))
							)
							{
								clearTimeout(t);
								clearInterval(move);
								pause = 1;
								document.getElementById("bg").style ='background-color:red';
								document.getElementById("settlement").style = 'visibility:visible';
								document.getElementById("settlement_time").innerHTML = time+1;
								document.getElementById("Winner").innerHTML = player3_name;
								document.getElementById("Loser").innerHTML = player2_name;	
							}
						}
						
						if(!select_ghost1){
							if (( -2 <= p1.offsetLeft && p1.offsetLeft <= 158) && ( -36 <= p1.offsetTop && p1.offsetTop <= 100)){ //safe1
								if(!player1_safe){
									player1_count++;
									document.getElementById("player1_count").innerHTML = 'Count: ' + player1_count;
								}
								player1_safe = 1;
							}
							else if ((700 <= p1.offsetLeft && p1.offsetLeft <=902) && ( -36 <= p1.offsetTop && p1.offsetTop <= 100)){ //safe2
								if(!player1_safe){
									player1_count++;
									document.getElementById("player1_count").innerHTML = 'Count: ' + player1_count;
								}
								player1_safe = 1;
							}
							else if (( -2 <=  p1.offsetLeft &&  p1.offsetLeft <= 158)&&( 504 <= p1.offsetTop && p1.offsetTop <= 652)){ //safe4
								if(!player1_safe){
									player1_count++;
									document.getElementById("player1_count").innerHTML = 'Count: ' + player1_count;
								}
								player1_safe = 1;							
							}
							else if((700 <=  p1.offsetLeft &&  p1.offsetLeft <=902)&&( 504 <= p1.offsetTop && p1.offsetTop <= 652)){ //safe3
								if(!player1_safe){
									player1_count++;
									document.getElementById("player1_count").innerHTML = 'Count: ' + player1_count;
								}
								player1_safe = 1;		
							}
							else{
								player1_safe = 0;
							}
							
						}
						if(!select_ghost2){
							if (( -2 <= p2.offsetLeft && p2.offsetLeft <= 158) && ( -36 <= p2.offsetTop && p2.offsetTop <= 100)){ //safe1
								if(!player2_safe){
									player2_count++;
									document.getElementById("player2_count").innerHTML = 'Count: ' + player2_count;
								}
								player2_safe = 1;
							}
							else if ((700 <= p2.offsetLeft && p2.offsetLeft <=902) && ( -36 <= p2.offsetTop && p2.offsetTop <= 100)){ //safe2
								if(!player2_safe){
									player2_count++;
									document.getElementById("player2_count").innerHTML = 'Count: ' + player2_count;
								}
								player2_safe = 1;
							}
							else if (( -2 <=  p2.offsetLeft &&  p2.offsetLeft <= 158)&&( 504 <= p2.offsetTop && p2.offsetTop <= 652)){ //safe4
								if(!player2_safe){
									player2_count++;
									document.getElementById("player2_count").innerHTML = 'Count: ' + player2_count;
								}
								player2_safe = 1;							
							}
							else if((700 <=  p2.offsetLeft &&  p2.offsetLeft <=902)&&( 504 <= p2.offsetTop && p2.offsetTop <= 652)){ //safe3
								if(!player2_safe){
									player2_count++;
									document.getElementById("player2_count").innerHTML = 'Count: ' + player2_count;
								}
								player2_safe = 1;		
							}
							else{
								player2_safe = 0;
							}
						}
						
						if(!select_ghost3){
							if (( -2 <= p3.offsetLeft && p3.offsetLeft <= 158) && ( -36 <= p3.offsetTop && p3.offsetTop <= 100)){ //safe1
								if(!player3_safe){
									player3_count++;
									document.getElementById("player3_count").innerHTML = 'Count: ' + player3_count;
								}
								player3_safe = 1;
							}
							else if ((700 <= p3.offsetLeft && p3.offsetLeft <=902) && ( -36 <= p3.offsetTop && p3.offsetTop <= 100)){ //safe2
								if(!player3_safe){
									player3_count++;
									document.getElementById("player3_count").innerHTML = 'Count: ' + player3_count;
								}
								player3_safe = 1;
							}
							else if (( -2 <=  p3.offsetLeft &&  p3.offsetLeft <= 158)&&( 504 <= p3.offsetTop && p3.offsetTop <= 652)){ //safe4
								if(!player3_safe){
									player3_count++;
									document.getElementById("player3_count").innerHTML = 'Count: ' + player3_count;
								}
								player3_safe = 1;							
							}
							else if((700 <=  p3.offsetLeft &&  p3.offsetLeft <=902)&&( 504 <= p3.offsetTop && p3.offsetTop <= 652)){ //safe3
								if(!player3_safe){
									player3_count++;
									document.getElementById("player3_count").innerHTML = 'Count: ' + player3_count;
								}
								player3_safe = 1;		
							}
							else{
								player3_safe = 0;
							}	
						}
					}	
				}
			}
		},50);
		
		
		time--;
	}
}

function m2() {
	clearTimeout(t);
	clearInterval(move);
	pause = 1;
}

function m3() {
	location.reload();
}

function getCoordinate(element) {
	let left = element.offsetLeft;
	let top = element.offsetTop;

	return [left, top];
}